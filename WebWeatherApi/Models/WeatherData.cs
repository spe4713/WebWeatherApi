﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebWeatherApi.Models
{
    public class WeatherData
    {
        public class Weather
        {
            // 地點
            public string localName { get; set; }
            public List<WeatherContent> Content { get; set; }
        }
        public class WeatherContent
        {
            //天氣敘述
            public string weatherScript { get; set; }

            //降雨機率
            public string pop { get; set; }

            //最低溫
            public string minTem { get; set; }

            //最高溫
            public string maxTem { get; set; }

            //開始日期
            public string beginTime { get; set; }

            //結束日期
            public string endTime { get; set; }
        }
    }
}