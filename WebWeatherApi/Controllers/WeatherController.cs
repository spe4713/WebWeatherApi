﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Data;
using WebWeatherApi.Models;

namespace WebWeatherApi.Controllers
{
    public class WeatherController : Controller
    {
        // GET: Weather
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetWeather(string Location)
        {
            List<WeatherData.Weather> data = new List<WeatherData.Weather>();
            HttpClient client = new HttpClient();
            string url = "";
            if (string.IsNullOrEmpty(Location))
            {
                url = "https://opendata.cwa.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=CWA-BEED2F13-2090-4AA9-A774-7E4AAFB2C306";
            }
            else
            {
                url = "https://opendata.cwa.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=CWA-BEED2F13-2090-4AA9-A774-7E4AAFB2C306&locationName=" + Location;
            }
            HttpResponseMessage msg = client.GetAsync(url).Result;
            // 取得JSON的字串
            string jsonAll = msg.Content.ReadAsStringAsync().Result.ToString();
            // 透過JsonConvert.DeserializeObject 將 JSON字串 轉成物件集合
            JObject weathers = JsonConvert.DeserializeObject<JObject>(jsonAll);
            // 將json資料轉變為json array陣列
            JArray arrAll = (JArray)weathers["records"]["location"];
            foreach (JObject m in arrAll)
            {
                List<WeatherData.WeatherContent> content = new List<WeatherData.WeatherContent>();
                content.Clear();
                //72小時資料
                for (int i = 0; i < 3; i++)
                {
                    content.Add(new WeatherData.WeatherContent
                    {
                        weatherScript = m["weatherElement"][0]["time"][i]["parameter"]["parameterName"].ToString(),
                        pop = m["weatherElement"][1]["time"][i]["parameter"]["parameterName"].ToString(),
                        minTem = m["weatherElement"][2]["time"][i]["parameter"]["parameterName"].ToString(),
                        maxTem = m["weatherElement"][4]["time"][i]["parameter"]["parameterName"].ToString(),
                        beginTime = Convert.ToDateTime(m["weatherElement"][0]["time"][i]["startTime"]).ToString("yyyy/MM/dd HH:mm"),
                        endTime = Convert.ToDateTime(m["weatherElement"][0]["time"][i]["endTime"]).ToString("yyyy/MM/dd HH:mm"),
                    });
                }
                data.Add(new WeatherData.Weather
                {
                    localName = (string)m["locationName"],
                    Content=content,
                });
            }
            return Json(JsonConvert.SerializeObject(data));
        }
    }
}